#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  8 11:28:37 2022

@author: claudio
"""

import numpy as np
import matplotlib.pyplot as plt

data_dir = '../postProcessing/sample/7460'

bench_dir = './Gartlin'

p14b = np.genfromtxt(bench_dir+'/p_14.txt')
p14d = np.genfromtxt(data_dir+'/x_by_h_14_p.xy')

p30b = np.genfromtxt(bench_dir+'/p_30.txt')
p30d = np.genfromtxt(data_dir+'/x_by_h_30_p.xy')



plt.figure()
plt.plot(p14b[:,1], p14b[:,0]*0.01, 'o', label='Gartlin')
plt.plot(p14d[:,1], p14d[:,0], lw=2, label='simulation')
plt.show()

plt.figure()
plt.plot(p30b[:,1], p30b[:,0]*0.01, 'o', label='Gartlin')
plt.plot(p30d[:,1], p30d[:,0], lw=2, label='simulation')
plt.show()
