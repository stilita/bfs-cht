#! /bin/bash
gnuplot -p << EOF
set grid
set title 'Heat Transfer Coefficient'
set xlabel 'Time [s]'
set ylabel 'heat transf'
# set style line 1 lt 2 lw 6
# set style line 1 lt 2 lw 6
# set linestyle  2 lt 2 lc 1 # red-dashed
# set linestyle  1 lt 2 lc 1 # red-dashed
plot "./precice-Fluid-watchpoint-x06.log" using 1:5 title "x 06 fluid" with lines, \
     "./precice-Fluid-watchpoint-x14.log" using 1:5 title "x 14 fluid" with lines, \
     "./precice-Fluid-watchpoint-x30.log" using 1:5 title 'x 30 fluid' with lines, \
     "./precice-Fluid-watchpoint-x60.log" using 1:5 title 'x 60 fluid' with lines, \
     "./precice-Fluid-watchpoint-x06.log" using 1:7 title "x 06 solid" with lines, \
     "./precice-Fluid-watchpoint-x14.log" using 1:7 title "x 14 solid" with lines, \
     "./precice-Fluid-watchpoint-x30.log" using 1:7 title 'x 30 solid' with lines, \
     "./precice-Fluid-watchpoint-x60.log" using 1:7 title 'x 60 solid' with lines
EOF

