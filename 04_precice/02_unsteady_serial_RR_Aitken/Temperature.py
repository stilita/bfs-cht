# -*- coding: utf-8 -*-
"""
Created on Tue Mar 21 09:56:02 2023

@author: claudio.caccia
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

yh = np.array([1.0,0.8,0.6,0.4,0.2,0.0,-0.2,-0.4,-0.6,-0.8,-1.0,-1.2,-1.4,-1.6,-1.8,-2.0,-2.2,-2.4,-2.6,-2.8,-3.0,-3.2,-3.4,-3.6,-3.8,-4.0,-4.2,-4.4,-4.6,-4.8,-5.0])

x06_k1=np.array([0.000,0.000,0.000,0.001,0.010,0.035,0.060,0.073,0.086,0.111,0.150,0.192,0.234,0.276,0.319,0.361,0.403,0.446,0.488,0.531,0.573,0.616,0.658,0.701,0.744,0.786,0.829,0.872,0.915,0.957,1.000])

x06_k10=np.array([0.000,0.000,0.000,0.003,0.036,0.141,0.255,0.328,0.400,0.512,0.665,0.681,0.697,0.713,0.730,0.746,0.763,0.780,0.796,0.813,0.830,0.847,0.864,0.881,0.898,0.915,0.932,0.949,0.966,0.983,1.000])

x06_k100=np.array([0.000,0.000,0.000,0.004,0.048,0.193,0.365,0.493,0.616,0.773,0.956,0.958,0.960,0.962,0.964,0.966,0.968,0.971,0.973,0.975,0.977,0.979,0.982,0.984,0.986,0.988,0.991,0.993,0.995,0.998,1.000])

x06_k1000=np.array([0.000,0.000,0.000,0.004,0.049,0.199,0.380,0.519,0.651,0.813,0.996,0.996,0.996,0.996,0.996,0.997,0.997,0.997,0.997,0.997,0.998,0.998,0.998,0.998,0.999,0.999,0.999,0.999,1.000,1.000,1.000])


x14_k1=np.array([0.000,0.000,0.000,0.000,0.000,0.001,0.003,0.009,0.020,0.042,0.084,0.130,0.176,0.222,0.269,0.315,0.361,0.406,0.452,0.498,0.544,0.590,0.635,0.681,0.727,0.772,0.818,0.863,0.909,0.954,1.000])

x14_k10=np.array([0.001,0.001,0.000,0.000,0.001,0.003,0.011,0.036,0.084,0.208,0.469,0.497,0.525,0.553,0.581,0.608,0.636,0.662,0.689,0.716,0.742,0.768,0.794,0.820,0.846,0.872,0.898,0.923,0.949,0.974,1.000])

x14_k100=np.array([0.001,0.001,0.001,0.001,0.001,0.004,0.015,0.050,0.122,0.364,0.899,0.904,0.910,0.915,0.921,0.926,0.931,0.937,0.942,0.947,0.952,0.957,0.962,0.967,0.971,0.976,0.981,0.986,0.991,0.995,1.000])

x14_k1000=np.array([0.001,0.001,0.001,0.001,0.001,0.004,0.016,0.051,0.128,0.396,0.989,0.990,0.990,0.991,0.991,0.992,0.993,0.993,0.994,0.994,0.995,0.995,0.996,0.996,0.997,0.997,0.998,0.998,0.999,0.999,1.000])



x30_k1=np.array([0.002,0.002,0.002,0.003,0.006,0.011,0.021,0.039,0.066,0.103,0.145,0.188,0.231,0.274,0.316,0.359,0.402,0.444,0.487,0.530,0.573,0.615,0.658,0.701,0.744,0.786,0.829,0.872,0.915,0.957,1.000])

x30_k10=np.array([0.006,0.007,0.008,0.013,0.024,0.049,0.098,0.182,0.307,0.467,0.643,0.661,0.679,0.697,0.715,0.732,0.750,0.768,0.786,0.804,0.822,0.839,0.857,0.875,0.893,0.911,0.929,0.946,0.964,0.982,1.000])

x30_k100=np.array([0.009,0.009,0.012,0.019,0.037,0.078,0.157,0.290,0.477,0.706,0.951,0.953,0.956,0.958,0.960,0.963,0.965,0.968,0.970,0.973,0.975,0.978,0.980,0.983,0.985,0.988,0.990,0.993,0.995,0.998,1.000])

x30_k1000=np.array([0.009,0.009,0.012,0.020,0.039,0.083,0.168,0.308,0.504,0.742,0.995,0.995,0.995,0.996,0.996,0.996,0.996,0.997,0.997,0.997,0.997,0.998,0.998,0.998,0.998,0.999,0.999,0.999,0.999,1.000,1.000])

Tmin = 300.0
Tmax = 310.0

H = 0.01

fs=14


deltaT = 4

finalT = 30

rangeT = np.arange(deltaT, finalT+deltaT, deltaT)

listT = []

for currT in rangeT:
    a = "{:.3f}".format(currT)
    #print(a)
    while(True):
        if a == '0':
            break
        elif a[-1] == '.':
            a = a[:-1]
            break
        elif a[-1] == '0':
            a = a[:-1]
        else:
            break
    listT.append(a)

#print(listT)

fig1 = plt.figure(figsize=(25,10))
ax1 = fig1.add_subplot(1,3,1)

plt.subplot(1,3,1)

plt.plot(x06_k1,yh,ls='',marker='h',ms=6,label='benchmark')

for i, currT in enumerate(listT):
    solid06 = np.genfromtxt('Solid/postProcessing/sample/'+currT+'/x_by_h_06.xy')
    fluid06 = np.genfromtxt('Fluid/postProcessing/sample/'+currT+'/x_by_h_06.xy')
    
    plt.plot((solid06[:,1]-Tmin)/(Tmax-Tmin), solid06[:,0]/H, label='t='+currT, color=[(i)/len(listT), 0,0])
    plt.plot((fluid06[:,1]-Tmin)/(Tmax-Tmin), fluid06[:,0]/H, color=[0,0,(i)/len(listT)])

patch= ax1.add_patch(patches.Rectangle((-0.1, -5), 1.2, 4, alpha=0.25,facecolor=[0.5,0.5,0.5],label='solid'))

plt.xlabel(r'$\theta$', fontsize=fs)
plt.ylabel(r'$\frac{y}{H}$',fontsize=fs)
plt.xlim([-0.1,1.1])
plt.ylim([-5,1])

plt.legend(fontsize=fs)
plt.title(r'$\frac{x}{H}=6$', fontsize=fs+2)

ax2 = fig1.add_subplot(1,3,2)

plt.plot(x14_k1,yh,ls='',marker='h',ms=6,label='benchmark')
    
for i, currT in enumerate(listT):
    solid14 = np.genfromtxt('Solid/postProcessing/sample/'+currT+'/x_by_h_14.xy')
    fluid14 = np.genfromtxt('Fluid/postProcessing/sample/'+currT+'/x_by_h_14.xy')

    plt.plot((solid14[:,1]-Tmin)/(Tmax-Tmin), solid14[:,0]/H, label='t='+currT, color=[(i)/len(listT), 0,0])
    plt.plot((fluid14[:,1]-Tmin)/(Tmax-Tmin), fluid14[:,0]/H, color=[0,0,(i)/len(listT)])

patch= ax2.add_patch(patches.Rectangle((-0.1, -5), 1.2, 4, alpha=0.25,facecolor=[0.5,0.5,0.5],label='solid'))

plt.xlabel(r'$\theta$', fontsize=fs)
plt.ylabel(r'$\frac{y}{H}$',fontsize=fs)
plt.xlim([-0.1,1.1])
plt.ylim([-5,1])

plt.legend(fontsize=fs)
plt.title(r'$\frac{x}{H}=14$', fontsize=fs+2)

ax3 = fig1.add_subplot(1,3,3)

plt.plot(x30_k1,yh,ls='',marker='h',ms=6,label='benchmark')

for i, currT in enumerate(listT):
    solid30 = np.genfromtxt('Solid/postProcessing/sample/'+currT+'/x_by_h_30.xy')
    fluid30 = np.genfromtxt('Fluid/postProcessing/sample/'+currT+'/x_by_h_30.xy')    
    
    plt.plot((solid30[:,1]-Tmin)/(Tmax-Tmin), solid30[:,0]/H, label='t='+currT, color=[(i)/len(listT), 0,0])
    plt.plot((fluid30[:,1]-Tmin)/(Tmax-Tmin), fluid30[:,0]/H, color=[0,0,(i)/len(listT)])

patch= ax3.add_patch(patches.Rectangle((-0.1, -5), 1.2, 4, alpha=0.25,facecolor=[0.5,0.5,0.5],label='solid'))

plt.xlabel(r'$\theta$', fontsize=fs)
plt.ylabel(r'$\frac{y}{H}$',fontsize=fs)
plt.xlim([-0.1,1.1])
plt.ylim([-5,1])

plt.legend(fontsize=fs)
plt.title(r'$\frac{x}{H}=30$', fontsize=fs+2)

plt.savefig('temperature.png')
plt.show()
