# BFS CHT

cases:

1. **fluid** for simulation of incompressible fluid flow
2. **HT-compressible** simulation of heat trasfer due to heated lower plate
3. **CHT** simulation of CHT in OpenFOAM10

### Notes:

- all cases have an **include** directory with an initialConditions file, to configure all parameters, in particular the number of control volumes **cvol**
- the file **MC3_CFDwOSS_report.pdf** is the course report with all the results obtained from the cases 01 02 and 03. Post-processing and images are in the repo [MC3](https://gitlab.com/stilita/MC3)

---

